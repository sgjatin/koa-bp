const Koa = require('koa');
const Router = require('koa-router')
const app = new Koa();
const router = new Router();

//Core init
require('./core/kernel')(app)
require('./core/error')(app)
require('dotenv').config()
// API routes
require('./app/routes')(router)
app.use(router.routes())
app.use(router.allowedMethods())

module.exports = app