# koa-bp

Follow the following steps to setup the project:
```
1) Run `make init`
2) Enter the `port` you want the application to run on
3) Open `docker-compose-development.yml` and replace the value of `APPPORT` with the port number you entered in previous step, also change value of `MONGOOUT` with port you want to expose the mongoDB service to your host.
4) Run `make build`
5) Run `make dev` and start development
```
Make commands list:
```
- `init` => Initiate the project
- `build` => Build project
- `dev` => Run project in dev mode
- `down` => Stop project
- `logs` => Show and follow logs of the project
- `restart` => Restart project in Dev mode
```