module.exports = (app) => {
    app.use(async (ctx, next) => {
        try {
            await next();
        } catch (err) {
            console.log(process.env.DEBUG_MODE)
            let errorResponse = {
                message: (process.env.DEBUG_MODE ? err.message: 'Something went wrong. Please contact support'),
                data: {}
            }
            ctx.status = err.status || 500;
            ctx.response.body = errorResponse;
            ctx.app.emit('error', err, ctx);
        }
    });

    app.on('error', (err, ctx) => {
        console.log('Error happened')
    });
}