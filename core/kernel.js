const bodyParser = require('koa-bodyparser');
const responseTime = require('koa-response-time');
module.exports = (app) => {
    // Init body parser
    app.use(bodyParser())

    // Init response time
    app.use(responseTime())
}