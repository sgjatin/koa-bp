const Router = require('koa-router')
const router = new Router()
const rootController = require('../controllers/root')
const auth = require('../middleware/auth')
const compose = require('koa-compose');

// Get routes
router.get('', rootController.status)

//Post routes
router.post('', compose([auth, rootController.statusPost]))

module.exports = router.routes()