let {response} = require('../../lib/response')
let root = {}
 /**
  * Decleration for status controller for APIs
  */

root.status = async function (ctx){
  try {
    ctx.response.status = 200
    ctx.response.message = "All system running, ready for launch"
  } catch (error) {
    throw error
  }
}

root.statusPost = async function (ctx) {
  try {
    let oResponse = {
      message: "All system running, ready for launch",
      data: {}
    }
    response(ctx, oResponse)
  } catch (error) {
    throw error
  }
}

module.exports = root