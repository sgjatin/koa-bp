#sed -i.bak 's/APPPORT/gghh/g' docker-compose-development.yml && rm -rf docker-compose-development.yml.bak
init:
	@echo "Initiating Project, please note it will reset any configuration made earlier"
	@rm -rf .env
	@rm -rf docker-compose-development.yml
	@cp docker-compose.yml docker-compose-development.yml
	@read -r -p "Application internal & exposed port => " APPPORT; \
	echo "PORT=$$APPPORT\nMONGO_URL=mongo:27017\nJWT_SECRET=KkiSS2U1SfdgyPhpwjtiw\nMONGO_DATABASE=ev\nDEBUG_MODE=true" > .env
	@echo "Application initiated"
build:
	@docker-compose -f docker-compose-development.yml build
dev:
	@docker-compose -f docker-compose-development.yml up -d
logs:
	@docker-compose -f docker-compose-development.yml logs -f
restart:
	@docker-compose -f docker-compose-development.yml down
	@docker-compose -f docker-compose-development.yml build
	@docker-compose -f docker-compose-development.yml up -d
down:
	@docker-compose -f docker-compose-development.yml down
development:
	@docker-compose -f docker-compose-development.yml up
