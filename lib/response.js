const response = async function (ctx, oResponse, statusCode) {
    try {
        ctx.response.status = statusCode || 404
        ctx.response.body = oResponse || {
            message: "Not found",
            data: {}
        }
    } catch (error) {
        throw error
    }
}

module.exports = {
    response: response
}